package com.ub.backend.article.module;

import com.ub.core.base.model.BaseModel;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

@Document
public class ArticleDoc extends BaseModel {
    @Id
    private ObjectId id;
    private String title;
    private String description;
    private ObjectId picId;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ObjectId getPicId() {
        return picId;
    }

    public void setPicId(ObjectId picId) {
        this.picId = picId;
    }

    public String getRussianCreatedAt(){
         return DateFormat.getDateInstance(SimpleDateFormat.LONG, new Locale("ru")).format(createdAt);
    }
}
