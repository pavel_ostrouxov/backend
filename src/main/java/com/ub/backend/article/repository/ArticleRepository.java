package com.ub.backend.article.repository;

import com.ub.backend.article.module.ArticleDoc;
import com.ub.core.base.repository.IRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import javax.annotation.Nullable;
import java.util.Date;
import java.util.List;

@Repository
public class ArticleRepository implements IRepository<ArticleDoc, ObjectId> {

    @Autowired private MongoTemplate mongoTemplate;

    @Override
    public ArticleDoc save(ArticleDoc articleDoc) {
        articleDoc.setUpdateAt(new Date());
        mongoTemplate.save(articleDoc);
        return articleDoc;
    }

    public List<ArticleDoc> findAll() {
        return mongoTemplate.findAll(ArticleDoc.class);
    }

    @Override
    public void remove(ObjectId objectId) {
        ArticleDoc articleDoc = findById(objectId);
        if(articleDoc != null){
            mongoTemplate.remove(articleDoc);
        }
    }

    @Nullable
    @Override
    public ArticleDoc findById(ObjectId objectId) {
        return mongoTemplate.findById(objectId, ArticleDoc.class);
    }

    @Override
    public boolean exist(Criteria criteria) {
        return mongoTemplate.exists(Query.query(criteria), ArticleDoc.class);
    }
}
