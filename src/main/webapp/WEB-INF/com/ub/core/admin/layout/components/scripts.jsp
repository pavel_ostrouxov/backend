<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script type="text/javascript" src="<c:url value="/static/ub/js/plugins/jquery-3.1.1.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/ub/js/materialize.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/ub/js/materialize-plugins/forms.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/ub/js/materialize-plugins/autocomplete/jquery.materialize-autocomplete.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/ub/js/plugins/prism/prism.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/ub/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/ub/js/plugins/sweetalert/dist/sweetalert.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/ub/js/plugins/dropify/js/dropify.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/ub/js/plugins/ckeditor/ckeditor.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/ub/js/plugins/ckeditor/adapters/jquery.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/ub/js/plugins.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/ub/js/custom-script.js"/>"></script>

<script type="text/javascript" src="<c:url value="/static/ub/js/sockjs/sockjs.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/static/ub/js/stompjs/stomp.min.js"/>"></script>
<tiles:importAttribute name="jsLinks"/>
<c:forEach items="${jsLinks}" var="jsLink">
    <script type="text/javascript" src="<c:url value="${jsLinks}"/>"></script>
</c:forEach>

<tiles:insertAttribute name="footer"/>