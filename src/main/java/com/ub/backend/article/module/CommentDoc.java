package com.ub.backend.article.module;

import com.ub.core.base.model.BaseModel;
import org.bson.types.ObjectId;

import javax.persistence.Id;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class CommentDoc extends BaseModel {
    @Id
    private ObjectId id;
    private String content;
    private ObjectId articleId;

    public ObjectId getArticleId() {
        return articleId;
    }

    public void setArticleId(ObjectId articleId) {
        this.articleId = articleId;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRussianCreatedAt(){
        return DateFormat.getDateInstance(SimpleDateFormat.LONG, new Locale("ru")).format(createdAt);
    }
}
