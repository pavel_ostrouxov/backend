package com.ub.backend.article.repository;

import com.ub.backend.article.module.ArticleDoc;
import com.ub.backend.article.module.CommentDoc;
import com.ub.core.base.repository.IRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import javax.annotation.Nullable;
import java.util.Date;
import java.util.List;

@Repository
public class CommentRepository implements IRepository<CommentDoc, ObjectId> {
    @Autowired private MongoTemplate mongoTemplate;

    @Override
    public CommentDoc save(CommentDoc commentDoc) {
        commentDoc.setUpdateAt(new Date());
        mongoTemplate.save(commentDoc);
        return commentDoc;
    }

    @Override
    public boolean exist(Criteria criteria) {
        return mongoTemplate.exists(Query.query(criteria), CommentDoc.class);
    }

    @Override
    public void remove(ObjectId objectId) {
        CommentDoc articleDoc = findById(objectId);
        if(articleDoc != null){
            mongoTemplate.remove(articleDoc);
        }
    }

    @Nullable
    @Override
    public CommentDoc findById(ObjectId objectId) {
        return mongoTemplate.findById(objectId, CommentDoc.class);
    }

    public List<CommentDoc> findByArticleId(ObjectId articleId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("articleId").is(articleId));
        return mongoTemplate.find(query, CommentDoc.class);
    }
}
