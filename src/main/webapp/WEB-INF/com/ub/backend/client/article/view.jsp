<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<section>
    <div class="container article-view-title">
        <div class="row">
            <h1 class="article-view-title-h1">
                ${articleDoc.title}
            </h1>
        </div>
    </div>
</section>

<section>
    <div class="container article-view-pic">
        <div class="row">
            <div class="col-md-12">
                <img class="article-view-pic-img" src="/pics/${articleDoc.picId}"/>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md12 article-view-content">
                ${articleDoc.description}
            </div>
        </div>
    </div>
</section>

<section>
    <form action="/article/add_comment/${articleDoc.id}" method="post">
        <div class="container article-view-comment-add">
            <div class="row">
                <div class="col-sm-1">
                    <div class="article-view-comment-add-userpic"></div>
                </div>

                <div class="col-sm-10">
                    <div class="article-view-comment-add-comment">
                        <input name="content" class="article-view-comment-add-comment-input">
                    </div>
                </div>

                <div class="col-sm-1">
                    <div class="article-view-comment-add-button-submit">
                        <button class="article-view-comment-add-submit-btn"></button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

<section>
    <div class="container article-view-comment-list">
        <div class="row">
            <c:forEach items="${comments}" var="commentDoc">
                <div class="col-md-12 article-view-comment-list-item">
                    <div class="article-view-comment-list-userpic">

                    </div>
                    <div class="article-view-comment-list-content">
                        <div class="article-view-comment-lst-content-date">
                            ${commentDoc.russianCreatedAt}
                        </div>
                        <div class="article-view-comment-lst-content-text">
                                ${commentDoc.content}
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</section>
