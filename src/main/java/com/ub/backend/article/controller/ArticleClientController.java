package com.ub.backend.article.controller;

import com.ub.backend.article.module.ArticleDoc;
import com.ub.backend.article.module.CommentDoc;
import com.ub.backend.article.repository.ArticleRepository;
import com.ub.backend.article.repository.CommentRepository;
import com.ub.backend.article.routes.ArticleClientRoute;
import com.ub.core.base.utils.RouteUtils;
import com.ub.core.picture.model.PictureDoc;
import com.ub.core.picture.repository.PictureRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class ArticleClientController {
    @Autowired private ArticleRepository articleRepository;
    @Autowired private PictureRepository pictureRepository;
    @Autowired private CommentRepository commentRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("articles", articleRepository.findAll());
        return "com.ub.backend.article.client.all";
    }

    @RequestMapping(value = ArticleClientRoute.ADD, method = RequestMethod.GET)
    public String add() {
        return "com.ub.backend.article.client.add";
    }

    @RequestMapping(value = ArticleClientRoute.SUCCESS, method = RequestMethod.GET)
    public String success() {
        return "com.ub.backend.article.client.success";
    }

    @RequestMapping(value = ArticleClientRoute.VIEW, method = RequestMethod.GET)
    public String view(@PathVariable ObjectId id, Model model) {
        model.addAttribute("articleDoc", articleRepository.findById(id));
        model.addAttribute("comments", commentRepository.findByArticleId(id));
        return "com.ub.backend.article.client.view";
    }

    @PostMapping(value = ArticleClientRoute.ADD)
    public String add( @RequestParam String title, @RequestParam String description, @RequestParam MultipartFile pic){

        PictureDoc pictureDoc = pictureRepository.save(pic);

        ArticleDoc articleDoc = new ArticleDoc();
        articleDoc.setPicId(pictureDoc.getId());
        articleDoc.setTitle(title);
        articleDoc.setDescription(description);
        articleRepository.save(articleDoc);

        articleDoc.setPicId(pictureDoc.getId());

        articleRepository.save(articleDoc);

        return RouteUtils.redirectTo(ArticleClientRoute.SUCCESS);
    }

    @PostMapping(value = ArticleClientRoute.ADD_COMMENT)
    public String addComment(@RequestParam String content, @PathVariable ObjectId id) {
        CommentDoc commentDoc = new CommentDoc();
        commentDoc.setArticleId(id);
        commentDoc.setContent(content);
        commentRepository.save(commentDoc);
        return RouteUtils.redirectTo(ArticleClientRoute.VIEW);
    }
}
