<div class="row">
    <div class="input-field col s12">
        <label for="seoTags-title">title</label>
        <input name="title" id="seoTags-title" value="${seoTags.title}" type="text"/>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <label for="seoTags-metaTitle">meta title</label>
        <input name="metaTitle" id="seoTags-metaTitle" value="${seoTags.metaTitle}" type="text"/>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <label for="seoTags-metaDescription">meta description</label>
        <input name="metaDescription" id="seoTags-metaDescription"
               value="${seoTags.metaDescription}" type="text"/>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <label for="seoTags-metaKeywords">meta keywords</label>
        <input name="metaKeywords" id="seoTags-metaKeywords" value="${seoTags.metaKeywords}" type="text"/>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <label for="seoTags-h1">H1</label>
        <input name="h1" id="seoTags-h1" value="${seoTags.h1}" type="text"/>
    </div>
</div>






