package com.ub.backend.article.controller;

import com.ub.backend.article.module.ArticleDoc;
import com.ub.backend.article.repository.ArticleRepository;
import com.ub.backend.article.routes.ArticleAdminRoutes;
import com.ub.core.base.utils.RouteUtils;
import com.ub.core.picture.model.PictureDoc;
import com.ub.core.picture.repository.PictureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ArticleAdminController {
    @Autowired private ArticleRepository articleRepository;
    @Autowired private PictureRepository pictureRepository;

    @GetMapping(ArticleAdminRoutes.ADD)
    public String add(Model model) {
        model.addAttribute("doc", new ArticleDoc());
        return "com.ub.backend.article.add";
    }

    @PostMapping(ArticleAdminRoutes.ADD)
    public String add(@ModelAttribute("doc") ArticleDoc articleDoc,
                      @RequestParam MultipartFile pic,
                      RedirectAttributes ra){
        PictureDoc pictureDoc = pictureRepository.save(pic);
        articleDoc.setPicId(pictureDoc.getId());

        articleRepository.save(articleDoc);
        ra.addAttribute("id", articleDoc.getId());
        return RouteUtils.redirectTo(ArticleAdminRoutes.EDIT);
    }
}
