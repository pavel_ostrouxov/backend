package com.ub.backend.base.routes;

import com.ub.core.base.routes.BaseRoutes;

public class BaseApiRoutes extends BaseRoutes {
    protected static final String API_V1 = API + "/v1";
    public static final String PICTURE = API_V1 + "/picture";
}
