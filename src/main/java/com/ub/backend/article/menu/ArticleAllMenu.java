package com.ub.backend.article.menu;

import com.ub.backend.article.routes.ArticleAdminRoutes;
import com.ub.core.base.menu.BaseMenu;

public class ArticleAllMenu extends BaseMenu {
    public ArticleAllMenu() {
        this.name = "Все";
        this.parent = new ArticleMenu();
        this.url = ArticleAdminRoutes.ALL;
    }
}
