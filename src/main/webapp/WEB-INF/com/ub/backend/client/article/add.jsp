<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="com.ub.backend.article.routes.ArticleClientRoute" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
</sectio>
    <div class="container">
        <form:form action="<%=ArticleClientRoute.ADD%>"
                   method="post" enctype="multipart/form-data">


                <div class="row">
                    <div class="col-md-10 offset-md-2 article-add-title">
                        <h1 class="article-add-title-h1">Добавление статьи</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8 offset-md-2 article-add-form">
                        <div class="article-add-form-group">
                            <label class="article-add-form-group-label">
                                Заголовок
                            </label>
                            <input name="title" class="article-add-form-group-input"/>
                        </div>
                        <div class="article-add-form-group">
                            <label class="article-add-form-group-label">
                                Описание
                            </label>
                            <textarea name="description" cols="40" class="article-add-form-group-input mod-textarea" rows="5"></textarea>
                            <%--<input name="description" class="article-add-form-group-input mod-textarea"/>--%>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 offset-md-2">
                        <div class="article-add-form-footer">
                            <label class="article-add-form-group-label">
                                Прикрепить изображение
                            </label>
                            <input type="file" name="pic" class="article-add-form-group-input mod-file" id="pic"/>
                            <label class="article-add-from-group-label-file" for="pic"></label>
                        </div>
                    </div>
                    <div class="col-md-4 article-add-form-footer-btn">
                        <a href="/" class="article-add-form-footer-btn-cancel">
                            Отмена
                        </a>
                        <button type="submit" class="article-add-from-footer-submit">
                            Добавить
                        </button>
                    </div>
                </div>
        </form:form>
    </div>
</section>