<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.ub.backend.article.routes.ArticleClientRoute" %>
<section>
    <div class="container header">
        <div class="row">
            <div class="col-md-6">
                <div class="header-logo">
                    <img src="/static/backend/img/logo.png" class="header-logo-img"/>
                </div>
                <div class="header-title">
                    Статьи, ${articles.size()}
                </div>
            </div>
            <div class="col-md-6">
                <div class="header-article-new">
                    <a href="<%= ArticleClientRoute.ADD%>" class="header-article-new-link">
                        <img class="header-article-new-link-img" src="/static/backend/img/article-new.png"/>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container article-list">
        <div class="row">

            <c:forEach items="${articles}" var="articleDoc">
                <div class="col-md-6">
                    <div class="article-list-item">
                        <a href="/article/view/${articleDoc.id}" class="article-list-item-link">
                            <div class="article-list-item-pic"
                                 style="background-image: url('/pics/${articleDoc.picId}')"></div>
                            <div class="article-list-item-title">
                                    ${articleDoc.title}
                            </div>
                            <div class="article-list-item-date">
                                ${articleDoc.russianCreatedAt}
                            </div>
                            <div class="article-list-item-preview">
                                ${articleDoc.description}
                            </div>
                        </a>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</section>